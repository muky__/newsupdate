//
//  SQLiteHelper.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 3/16/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import Foundation
import SQLite

class SQLiteHelper {
    var database : Connection!
    
    var user : User?
    
    let usersTable = Table("users")
    let userId = Expression<Int>("id")
    let username = Expression<String>("username")
    let password = Expression<String>("password")
    let firstName = Expression<String>("first_name")
    let lastName = Expression<String>("last_name")
    let address = Expression<String>("address")
    let email = Expression<String>("email")
    let contact = Expression<String>("contact")
    
    let newsTable = Table("news")
    let newsId = Expression<Int>("id")
    let newsTitle = Expression<String>("title")
    let desc = Expression<String>("description")
    let author = Expression<String>("author")
    
    func connectDB() {
        do {
            let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileUrl = documentDirectory.appendingPathComponent("users").appendingPathExtension("sqlite3")
            database = try Connection(fileUrl.path)
        } catch {
            print(error)
        }
    }
    
    func createUserTable() {
        let createTable = self.usersTable.create { (table) in
            table.column(self.userId, primaryKey: true)
            table.column(self.username, unique: true)
            table.column(self.password)
            table.column(self.firstName)
            table.column(self.lastName)
            table.column(self.address)
            table.column(self.email, unique: true)
            table.column(self.contact)
        }
        
        do {
            try self.database.run(createTable)
            UserDefaults.standard.set(true, forKey: "isTableCreated")
            print("Created Users Table")
        } catch {
            print(error)
        }
    }
    
    func insertUser(username : String, password : String, email : String, contact : String, firstName : String, lastName : String, address : String) -> Bool {
        let insertUser = self.usersTable.insert(self.username <- username, self.password <- password, self.firstName <- firstName, self.lastName <- lastName, self.address <- address, self.email <- email, self.contact <- contact)
    
        do {
                try self.database.run(insertUser)
                print("INSERTED USER")
                return true
        }catch {
            print(error)
        }
        return false
    }
    
    func checkUsernameExistence(uname : String) -> Bool{
        do {
            let users = try self.database.prepare(self.usersTable)
            for user in users {
                if user[self.username] == uname{
                    return true
                }
            }
        } catch {
            print(error)
        }
        return false
    }
    
    func checkEmailExistence(email : String) -> Bool{
        do {
            let users = try self.database.prepare(self.usersTable)
            for user in users {
                if user[self.email] == email{
                    return true
                }
            }
        } catch {
            print(error)
        }
        return false
    }
    
    func checkLoginCredentials(uname : String, pass : String) -> Bool{
        do {
            let users = try self.database.prepare(self.usersTable)
            for user in users {
                if user[self.username] == uname && user[self.password] == pass{
                    self.user = User(id: user[self.userId], username: uname, password: pass, email: user[self.email], contact: user[self.contact], firstName: user[self.firstName], lastName: user[self.lastName], address: user[self.address])
                    return true
                }
            }
        } catch {
            print(error)
        }
        return false
    }
    
    func updateUser(username : String, firstName: String, lastName: String, address: String, contact: String) -> Bool {
        let user = self.usersTable.filter(self.username == username)
        let updateUser = user.update(self.firstName <- firstName, self.lastName <- lastName, self.address <- address, self.contact <- contact)
        do {
            try self.database.run(updateUser)
            updateUserInfoInUserDefaults(uname: username)
            return true
        } catch {
            print(error)
        }
        return false
    }
    
    func updateUserInfoInUserDefaults(uname : String){
        do {
            let users = try self.database.prepare(self.usersTable)
            for user in users {
                if user[self.username] == uname {
                    self.user = User(id: user[self.userId], username: uname, password: user[self.password], email: user[self.email], contact: user[self.contact], firstName: user[self.firstName], lastName: user[self.lastName], address: user[self.address])
                    if let encoded = try? JSONEncoder().encode(self.user) {
                        UserDefaults.standard.set(encoded, forKey: "user")
                    }
                }
            }
        } catch {
            print(error)
        }
    }
    
    func updatePassword(username : String, oldPassword : String, newPassword: String, confirmPassword: String) -> Bool {
        let user = self.usersTable.filter(self.username == username)
        let updatePassword = user.update(self.password <- newPassword)
        do {
            try self.database.run(updatePassword)
            updateUserInfoInUserDefaults(uname: username)
            return true
        } catch {
            print(error)
        }
        return false
    }
    
    func connectNewsDB() {
        do {
            let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileUrl = documentDirectory.appendingPathComponent("news").appendingPathExtension("sqlite3")
            let database = try Connection(fileUrl.path)
            self.database = database
        } catch {
            print(error)
        }
    }
    
    func createNewsTable() {
        let createTable = self.newsTable.create { (table) in
            table.column(self.newsId, primaryKey: true)
            table.column(self.newsTitle)
            table.column(self.desc)
            table.column(self.author)
        }
        
        do {
            try self.database.run(createTable)
            UserDefaults.standard.set(true, forKey: "isNewsTableCreated")
            print("Created News Table")
        } catch {
            print(error)
        }
    }
    
    func insertNews(title : String, description : String, author : String) -> Bool{
        let insertNews = self.newsTable.insert(self.newsTitle <- title, self.desc <- description, self.author <- author)
        
        do {
            try self.database.run(insertNews)
            print("NEWS INSERTED")
            return true
        } catch {
            print(error)
        }
        return false
    }
    
    func fetchNews() -> [LocalNews]{
        var newsList = [LocalNews]()
        do {
            let newsData = try self.database.prepare(self.newsTable)
            for news in newsData {
                newsList.append(LocalNews(id: news[self.newsId], title: news[self.newsTitle], desc: news[self.desc], author: news[self.author]))
            }
        } catch {
            print(error)
        }
        return newsList
    }
    
    func deleteNews(id : Int) -> Bool {
        let news = self.newsTable.filter(self.newsId == id)
        let deleteNews = news.delete()
        do {
            try self.database.run(deleteNews)
            return true
        } catch {
            print(error)
        }
        return false
    }
    
    func updateNews(id : Int, title : String, description : String) -> Bool{
        let news = self.newsTable.filter(self.newsId == id)
        let updateNews = news.update(self.newsTitle <- title, self.desc <- description)
        do {
            try self.database.run(updateNews)
            return true
        } catch {
            print(error)
        }
        return false
    }
}
