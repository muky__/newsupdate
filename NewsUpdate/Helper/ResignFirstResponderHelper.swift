//
//  ResignFirstResponderHelper.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 3/16/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import Foundation
import UIKit

class ResignFirstResponderHelper{
    func resign(textfields : [UITextField]) {
        for textfield in textfields {
            textfield.resignFirstResponder()
        }
    }
}
