//
//  FetchingImageHelper.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 3/19/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import Foundation
import UIKit
class FetchingImageHelper {
    func get_image(_ url_str:String, _ imageView:UIImageView) {
        let url:URL = URL(string: url_str)!
        let session = URLSession.shared
        let task = session.dataTask(with: url, completionHandler: { (
            data, response, error) in
            if data != nil {
                let image = UIImage(data: data!)
                if(image != nil) {
                    DispatchQueue.main.async(execute: {
                        imageView.image = image
                        imageView.alpha = 0
                        UIView.animate(withDuration: 2.5, animations: { imageView.alpha = 1.0
                        }) })
                } }
        })
        task.resume() }
}
