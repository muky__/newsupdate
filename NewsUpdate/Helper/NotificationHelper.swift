//
//  NotificationHelper.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 2/28/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import Foundation
import UserNotifications
class NotificationHelper {
    func notify(title : String, body : String) {
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            content.title = title
            content.body = body
            content.sound = UNNotificationSound.default()
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)
            let request = UNNotificationRequest(identifier: "localNotification", content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request) { (error) in
                print(error)
            }
        } else {
            // Fallback on earlier versions
        }
        
    }
}
