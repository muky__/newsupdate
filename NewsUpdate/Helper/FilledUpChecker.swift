//
//  FilledUpChecker.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 3/6/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import Foundation
import UIKit
class FilledUpChecker {
    func check(textfields : [UITextField]) -> Bool {
        for textfield in textfields {
            if (textfield.text?.isEmpty)! {
                return false
            }
        }
        return true
    }
}
