//
//  TextFieldCleaner.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 2/28/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import Foundation
import UIKit
class TextFieldCleaner {
    func clearAllTextFields(textfields : [UITextField]) {
        for textfield in textfields {
            textfield.text = ""
        }
    }
}
