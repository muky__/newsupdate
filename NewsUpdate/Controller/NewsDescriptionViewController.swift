//
//  NewsDescriptionViewController.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 2/28/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import UIKit

class NewsDescriptionViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsTableView: UITableView!
    
    var newsTitle = ""
    var desc = ""
    var imageUrl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        newsTableView.dataSource = self
        newsTableView.delegate = self
        
        setupNavBar()
        loadImage()

        // Do any additional setup after loading the view.
    }
    
    func setupNavBar() {
        self.title = "News Detail"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "left-arrow.png"), style: .plain, target: self, action: #selector(popBack))
    }
    
    @objc func popBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadImage() {
//        let url = URL(string: imageUrl)
//        DispatchQueue.global(qos: .userInitiated).async {
//            do{
//                let imageData : Data = try Data(contentsOf: url!)
//                DispatchQueue.main.async {
//                    self.newsImageView.image = UIImage(data: imageData)
//                }
//            }catch {
//                print(error)
//            }
//        }
        let fetchingImageHelper = FetchingImageHelper()
        fetchingImageHelper.get_image(imageUrl, newsImageView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "singleNewsCell", for: indexPath) as! NewsTableViewCell
            cell.titleLabel.text = self.newsTitle
            cell.titleLabel.font = cell.titleLabel.font.bold()
            cell.descriptionLabel.text = self.desc.htmlToString
            return cell
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension UIFont {
    func withTraits(traits:UIFontDescriptorSymbolicTraits...) -> UIFont {
        let descriptor = self.fontDescriptor
            .withSymbolicTraits(UIFontDescriptorSymbolicTraits(traits))
        return UIFont(descriptor: descriptor!, size: 0)
    }
    
    func bold() -> UIFont {
        return withTraits(traits: .traitBold)
    }
}
