//
//  DashboardTableViewController.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 2/28/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class DashboardTableViewController: UITableViewController {
    
    var newsArray = [News]()
    var eventList = [Event]()
    
    let validationHelper = ValidationHelper()
    let alertHelper = AlertHelper()
    let fetchingImageHelper = FetchingImageHelper()
    let dropdown = DropDown()

    @IBOutlet var newsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        self.refreshControl = UIRefreshControl()
//        self.refreshControl?.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Refreshing event data")
        self.refreshControl?.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
    }
    
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        print("data refreshed")
        if Reachability.isConnectedToNetwork(){
            pullRefresh()
            print("Internet Connection Available!")
        }else {
            let alert = UIAlertController(title: "Error", message: "No internet connection", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                self.refreshControl?.endRefreshing()
            }
            alert.addAction(okAction)
            self.present(alert, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        fetchData()
        if Reachability.isConnectedToNetwork(){
            fetchEventData()
            print("Internet Connection Available!")
        }else{
            alertHelper.popAlert(parentViewController: self, title: "Error", message: "No internet connection")
        }
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        SVProgressHUD.dismiss()
    }
    
    func setupNavBar(){
        self.title = "News Feed"
        
        dropdown.anchorView = self.view
        dropdown.dataSource = ["View local news", "Profile", "Change password", "Logout"]
        dropdown.width = 200
        dropdown.direction = .bottom
        dropdown.bottomOffset = CGPoint(x: ((dropdown.anchorView?.plainView.bounds.width)! - 200), y: 65)
        
        //create a new button
        let button = UIButton.init(type: .custom)
        //set image for button
        button.setImage(UIImage(named: "more.png"), for: UIControlState.normal)
        //add function for button
        button.addTarget(self, action: #selector(DashboardTableViewController.popupDropDown), for: UIControlEvents.touchUpInside)
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.rightBarButtonItem = barButton
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "more.png"), style: .plain, target: self, action: #selector(popupDropDown))
        
        dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
//            print("Selected item: \(item) at index: \(index)")
            switch index {
            case 0:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "newsFeedTVC") as! NewsFeedTableViewController
                self.navigationController?.pushViewController(vc, animated: true)
            case 1:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "profileVC") as! ProfileViewController
                self.navigationController?.pushViewController(vc, animated: true)
            case 2:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "changePasswordTVC") as! ChangePasswordTableViewController
                self.navigationController?.pushViewController(vc, animated: true)
            case 3:
                self.signout()
            default:
                print("Error selecting drop down")
            }
            self.dropdown.clearSelection()
        }
        
        
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Log out", style: .plain, target: self, action: #selector(signout))
    }
    
    @objc func popupDropDown() {
        dropdown.show()
    }
    
    func signout(){
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        UserDefaults.standard.set("", forKey: "username")
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "loginVC") as! LoginViewController
        self.present(loginVC, animated: true, completion: nil)
    }
    
    func fetchData() {
        self.newsArray.removeAll()
        self.navigationItem.hidesBackButton = true
        SVProgressHUD.show(withStatus: "Fetching news")
        SVProgressHUD.setStatus("Fetching Data")
        let params = [
            "token" : "147258369123456789",
            "content_type" : "News"
        ]
        Alamofire.request("http://esmart.almawadait.com/api/content/lists", method: .post, parameters: params).responseJSON { (response) in
            if response.result.isSuccess {
//                print(response)
                let data = response.result.value as! NSDictionary
                if data["status"] as! Int == 200 {
                    if let records = data["records"] as? NSArray {
                        for record in records {
                            self.newsArray.append(News.init(json: record as! NSDictionary))
                        }
                    }
                    self.newsTableView.reloadData()
                    SVProgressHUD.dismiss()
                }
            }
            else {
                SVProgressHUD.dismiss()
                self.alertHelper.popAlert(parentViewController: self, title: "Error", message: "Error while fetching data")
                self.newsTableView.reloadData()
            }
        }
    }
    
    func fetchEventData() {
        eventList.removeAll()
        self.newsTableView.allowsSelection = false
        self.navigationItem.hidesBackButton = true
        SVProgressHUD.show(withStatus: "Fetching events")
        SVProgressHUD.setStatus("Fetching Data")
        let params = [
            "token" : "i3rq9jj9f2wy42bxldknnzr7o77pyzfi87yx0gjm"
        ]
        Alamofire.request("http://api.yeahviva.com/Events/getEventListingForWheel.json", method: .post, parameters: params).responseJSON { (response) in
            if response.result.isSuccess {
                let data = response.result.value as! NSDictionary
                if let output = data["output"] as? NSDictionary {
                    if let response = output["response"] as? NSArray {
                        for res in response {
                            self.eventList.append(Event(json: (res as? NSDictionary)!))
                        }
                        self.newsTableView.reloadData()
                        self.newsTableView.allowsSelection = true
                        SVProgressHUD.dismiss()
                    }
                }
            }else {
                SVProgressHUD.dismiss()
                self.alertHelper.popAlert(parentViewController: self, title: "Error", message: "Error while fetching data")
            }
        }
    }
    
    func pullRefresh() {
        eventList.removeAll()
        self.newsTableView.reloadData()
        self.newsTableView.allowsSelection = false
        self.navigationItem.hidesBackButton = true
        let params = [
            "token" : "i3rq9jj9f2wy42bxldknnzr7o77pyzfi87yx0gjm"
        ]
        Alamofire.request("http://api.yeahviva.com/Events/getEventListingForWheel.json", method: .post, parameters: params).responseJSON { (response) in
            if response.result.isSuccess {
                let data = response.result.value as! NSDictionary
                if let output = data["output"] as? NSDictionary {
                    if let response = output["response"] as? NSArray {
                        for res in response {
                            self.eventList.append(Event(json: (res as? NSDictionary)!))
                        }
                        self.newsTableView.reloadData()
                        self.newsTableView.allowsSelection = true
                        self.refreshControl?.endRefreshing()
                    }
                }
            }else {
                self.alertHelper.popAlert(parentViewController: self, title: "Error", message: "Error while fetching data")
            }
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
//        return self.newsArray.count
        return self.eventList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as! NewsFeedTableViewCell
        cell.titleLabel.text = eventList[indexPath.row].eventName
        cell.createdAtLabel.text = eventList[indexPath.row].start_date
        fetchingImageHelper.get_image(eventList[indexPath.row].mainImage!, cell.newImageView)

//        cell.titleLabel.text = newsArray[indexPath.row].content_title
//        cell.createdAtLabel.text = newsArray[indexPath.row].created
//
//        let url = URL(string: newsArray[indexPath.row].featured_img!)
        
//        let url = URL(string: eventList[indexPath.row].mainImage!)
//        DispatchQueue.global(qos: .userInitiated).async {
//            do{
//                let imageData : Data = try Data.init(contentsOf: url!)
//                DispatchQueue.main.async {
//                    cell.newImageView.image = UIImage.init(data: imageData)
//                }
//            }catch {
//                print(error)
//            }
//        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "detailVC") as! NewsDescriptionViewController
        detailVC.imageUrl = self.eventList[indexPath.row].mainImage!
        detailVC.newsTitle = self.eventList[indexPath.row].eventName!
        detailVC.desc = self.eventList[indexPath.row].eventDescription!
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}
