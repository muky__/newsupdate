//
//  RegisterationTableViewController.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 3/16/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import UIKit
import UserNotifications

class RegisterationTableViewController: UITableViewController, UNUserNotificationCenterDelegate {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var rePasswordTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var contactTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    
    let alertHelper = AlertHelper()
    let notificationHelper = NotificationHelper()
    let filledUpChecker = FilledUpChecker()
    let textFieldCleaner = TextFieldCleaner()
    let validationHelper = ValidationHelper()
    let sqliteHelper = SQLiteHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sqliteHelper.connectDB()
        if !(UserDefaults.standard.bool(forKey: "isTableCreated")){
            sqliteHelper.createUserTable()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func setupUI() {
        self.title = "Registeration"
        registerButton.layer.cornerRadius = 15
        resetButton.layer.cornerRadius = 15
        usernameTextField.becomeFirstResponder()
    }
    
    @IBAction func registerButtonClicked(_ sender: UIButton) {
        if filledUpChecker.check(textfields: [
            usernameTextField,
            passwordTextField,
            rePasswordTextField,
            firstNameTextField,
            lastNameTextField,
            addressTextField,
            emailTextField,
            contactTextField]) {
            
            if passwordTextField.text == rePasswordTextField.text {
                if validationHelper.isValidEmail(email: emailTextField.text){
                    let username = usernameTextField.text!
                    let password = passwordTextField.text!
                    let firstName = firstNameTextField.text!
                    let lastName = lastNameTextField.text!
                    let address = addressTextField.text!
                    let email = emailTextField.text!
                    let contact = contactTextField.text!
                    
                    if contact.isPhoneNumber{
                        if !(sqliteHelper.checkUsernameExistence(uname: username)) {
                            if !(sqliteHelper.checkEmailExistence(email: email)) {
                                if !(sqliteHelper.insertUser(username: username, password: password, email: email, contact: contact, firstName: firstName, lastName: lastName, address: address)) {
                                        alertHelper.popAlert(parentViewController: self, title: "Error", message: "User could not be inserted")
                                }else {
                                    notificationHelper.notify(title: "Success", body: "You are now an official user")
                                    performSegue(withIdentifier: "loginSegue", sender: self)
                                }
                            }else {
                                alertHelper.popAlert(parentViewController: self, title: "Email already used", message: "Please use another email address")
                            }
                        }else {
                            alertHelper.popAlert(parentViewController: self, title: "Username already exists", message: "Please try another username")
                        }
                    }else {
                        alertHelper.popAlert(parentViewController: self, title: "Invalid contact number", message: "Please enter a valid contact number")
                    }
                }else {
                    alertHelper.popAlert(parentViewController: self, title: "Wrong email address", message: "Please enter a valid email address")
                }
            }else {
                alertHelper.popAlert(parentViewController: self, title: "Password mismatch", message: "Please retype the password")
            }
        }else {
            alertHelper.popAlert(parentViewController: self, title: "Missing information", message: "Please fill up all the fields")
        }
    }

    @IBAction func resetButtonClicked(_ sender: UIButton) {
        textFieldCleaner.clearAllTextFields(textfields: [
            usernameTextField,
            passwordTextField,
            rePasswordTextField,
            firstNameTextField,
            lastNameTextField,
            addressTextField,
            emailTextField,
            contactTextField
            ])
    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
}


