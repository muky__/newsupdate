//
//  EditProfileTableViewController.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 3/12/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import UIKit
import UserNotifications

class EditProfileTableViewController: UITableViewController, UNUserNotificationCenterDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var contactTextField: UITextField!
    @IBOutlet weak var updateButton: UIButton!
    
    var imagePicker = UIImagePickerController()
    
    let filledUpChecker = FilledUpChecker()
    let alertHelper = AlertHelper()
    let sqliteHelper = SQLiteHelper()
    let notificationHelper = NotificationHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        imagePicker.delegate = self
        
        sqliteHelper.connectDB()
        populateData()
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        } else {
            // Fallback on earlier versions
        }
    }
    
    func setupUI() {
        firstNameTextField.borderStyle = .roundedRect
        lastNameTextField.borderStyle = .roundedRect
        addressTextField.borderStyle = .roundedRect
        contactTextField.borderStyle = .roundedRect
        updateButton.layer.cornerRadius = 5
        setupNavBar()
        displaySavedImage()
        setupGestureInImageView()
    }
    
    func setupGestureInImageView() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(profileImageTapped(tapGestureRecognizer:)))
        profileImageView.isUserInteractionEnabled = true
        profileImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func profileImageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let alert = UIAlertController(title: "Choose Photo", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func displaySavedImage() {
        let data = UserDefaults.standard.object(forKey: "savedImage") as! Data
        profileImageView.image = UIImage(data: data)
    }
    
    func saveImage(image : UIImage) {
        let data : Data = UIImageJPEGRepresentation(image, 1)!
        UserDefaults.standard.set(data, forKey: "savedImage")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func populateData() {
        if let userData = UserDefaults.standard.object(forKey: "user") {
            if let user = try? JSONDecoder().decode(User.self, from: userData as! Data) {
                firstNameTextField.text = user.firstName
                lastNameTextField.text = user.lastName
                addressTextField.text = user.address
                contactTextField.text = user.contact
            }
        }
    }
    
    func setupNavBar() {
        self.title = "Edit Profile"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "left-arrow.png"), style: .plain, target: self, action: #selector(popBack))
    }
    
    @objc func popBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func updateButtonTapped(_ sender: UIButton) {
        if filledUpChecker.check(textfields: [
                                firstNameTextField,
                                lastNameTextField,
                                addressTextField,
                                contactTextField]) {
            if sqliteHelper.updateUser(username: UserDefaults.standard.string(forKey: "username")!, firstName: firstNameTextField.text!, lastName: lastNameTextField.text!, address: addressTextField.text!, contact: contactTextField.text!) {
                    saveImage(image: profileImageView.image!)
                    notificationHelper.notify(title: "Success", body: "Your profile has been updated")
                    self.navigationController?.popViewController(animated: true)
            }
        } else {
            alertHelper.popAlert(parentViewController: self, title: "Missing information", message: "Please fill up all the fields")
        }
    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profileImageView.image = pickedImage
//            saveImage(image: pickedImage)
//            displaySavedImage()
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
