//
//  ProfileViewController.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 3/9/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
    
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        imagePicker.delegate = self
//        saveImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        populateData()
        if UserDefaults.standard.object(forKey: "savedImage") != nil {
            displaySavedImage()
        }
    }
    
    func populateData() {
        if let userData = UserDefaults.standard.object(forKey: "user") {
            if let user = try? JSONDecoder().decode(User.self, from: userData as! Data) {
                usernameLabel.text = user.username
                firstNameLabel.text = "First name: \(user.firstName)"
                lastNameLabel.text = "Last name: \(user.lastName)"
                addressLabel.text = "Address: \(user.address)"
                emailLabel.text = "Email: \(user.email)"
                contactLabel.text = "Contact: \(user.contact)"
            }
        }
    }

    func setupUI() {
        setupNavBar()
        profileImageView.layer.cornerRadius = profileImageView.layer.frame.width / 2
        profileImageView.clipsToBounds = true
//        setupGestureInImageView()
    }

    func setupNavBar() {
        self.title = "Profile"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "left-arrow.png"), style: .plain, target: self, action: #selector(popBack))
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(popEditProfile))
        let editButton = UIButton(type: .custom)
        editButton.setTitle("Edit", for: .normal)
        editButton.setTitleColor(UIColor.black, for: .normal)
        editButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        editButton.addTarget(self, action: #selector(popEditProfile), for: .touchUpInside)
        let editItem = UIBarButtonItem(customView: editButton)
        self.navigationItem.rightBarButtonItem = editItem
    }
    
    func setupGestureInImageView() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(profileImageTapped(tapGestureRecognizer:)))
        profileImageView.isUserInteractionEnabled = true
        profileImageView.addGestureRecognizer(tapGestureRecognizer)
//        coverImageView.isUserInteractionEnabled = true
//        coverImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func saveImage(image : UIImage) {
        let data : Data = UIImageJPEGRepresentation(image, 1)!
        UserDefaults.standard.set(data, forKey: "savedImage")
    }
    
    func displaySavedImage() {
        let data = UserDefaults.standard.object(forKey: "savedImage") as! Data
        profileImageView.image = UIImage(data: data)
        coverImageView.image = UIImage(data: data)
    }
    
    @objc func profileImageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let alert = UIAlertController(title: "Choose Photo", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func popEditProfile() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "editProfileTVC") as! EditProfileTableViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func popBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            saveImage(image: pickedImage)
            displaySavedImage()
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
