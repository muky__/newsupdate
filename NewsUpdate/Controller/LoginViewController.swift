//
//  LoginViewController.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 2/28/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import UIKit
import Toast_Swift

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    let alertHelper = AlertHelper()
    let sqliteHelper = SQLiteHelper()
    let textFieldCleaner = TextFieldCleaner()
    let filledUpChecker = FilledUpChecker()
    let resignFirstResponderHelper = ResignFirstResponderHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sqliteHelper.connectDB()
        usernameTextField.becomeFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func setupUI(){
        setupNavBar()
        usernameTextField.borderStyle = .roundedRect
        passwordTextField.borderStyle = .roundedRect
        loginButton.layer.cornerRadius = 5
        registerButton.layer.cornerRadius = 5
    }
    
    func setupNavBar() {
        self.title = "Login"
        self.navigationItem.hidesBackButton = true
    }

    @IBAction func loginButtonClicked(_ sender: UIButton) {
        resignFirstResponderHelper.resign(textfields: [usernameTextField, passwordTextField])
        if filledUpChecker.check(textfields: [usernameTextField, passwordTextField]) {
            if sqliteHelper.checkLoginCredentials(uname: usernameTextField.text!, pass: passwordTextField.text!) {
                UserDefaults.standard.set(true, forKey: "isLoggedIn")
                UserDefaults.standard.set(usernameTextField.text, forKey: "username")
                if let encoded = try? JSONEncoder().encode(sqliteHelper.user) {
                    UserDefaults.standard.set(encoded, forKey: "user")
                }
                textFieldCleaner.clearAllTextFields(textfields: [
                        usernameTextField,
                        passwordTextField
                    ])
                performSegue(withIdentifier: "dashboardSegue", sender: self)
            }else {
                self.view.makeToast("Wrong username or password", position: .top, title: "Wrong credential", image: UIImage.init(named: "error.png"), completion: { (success) in
                    
                })
            }
        }else {
            alertHelper.popAlert(parentViewController: self, title: "Missing information", message: "Both username and password are required to perform login action")
        }
    }
}
