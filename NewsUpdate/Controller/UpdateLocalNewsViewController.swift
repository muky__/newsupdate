//
//  UpdateLocalNewsViewController.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 3/6/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import UIKit
import UserNotifications

class UpdateLocalNewsViewController: UIViewController, UNUserNotificationCenterDelegate {
    
    var titleString = ""
    var descString = ""
    var id = 0

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var updateNewsButton: UIButton!
    
    let alertHelper = AlertHelper()
    let notificationHelper = NotificationHelper()
    let sqliteHelper = SQLiteHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        sqliteHelper.connectNewsDB()
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func setupUI() {
        titleTextField.text = titleString
        descriptionTextView.text = descString
        
        titleTextField.borderStyle = .roundedRect
        
        updateNewsButton.layer.cornerRadius = 5
        
        //adding border in textview as in textfield
        descriptionTextView.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        descriptionTextView.layer.borderWidth = 1.0;
        descriptionTextView.layer.cornerRadius = 5.0;
        
        setupNavBar()
    }
    
    func setupNavBar() {
        self.title = "Edit news"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "left-arrow.png"), style: .plain, target: self, action: #selector(popBack))
    }
    
    @IBAction func updateNewsButtonTapped(_ sender: UIButton) {
        if isAllFilledUp() {
            if sqliteHelper.updateNews(id: self.id, title: titleTextField.text!, description: descriptionTextView.text) {
                notificationHelper.notify(title: "Success", body: "News has been updated")
                goBack()
            }else {
                notificationHelper.notify(title: "Failure", body: "Something went wrong")
            }
        }else {
            alertHelper.popAlert(parentViewController: self, title: "Missing information", message: "Please fill up all the fields")
        }
    }
    
    func isAllFilledUp() -> Bool {
        if (titleTextField.text?.isEmpty)! || (descriptionTextView.text.isEmpty) {
            return false
        }
        return true
    }
    
    @objc func popBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func goBack() {
        let viewContollers : [UIViewController] = self.navigationController!.viewControllers as! [UIViewController]
        self.navigationController?.popToViewController(viewContollers[viewContollers.count - 3], animated: true)
    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
}
