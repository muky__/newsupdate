//
//  LocalNewsDetailViewController.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 3/6/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import UIKit
import UserNotifications

class LocalNewsDetailViewController: UIViewController, UNUserNotificationCenterDelegate {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    var newsTitleString = ""
    var descString = ""
    var id = 0
    var amIAuthor = true
    
    let notificationHelper = NotificationHelper()
    let sqliteHelper = SQLiteHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sqliteHelper.connectNewsDB()
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        } else {
            // Fallback on earlier versions
        }
        setupUI()
    }
    
    func setupUI() {
        titleLabel.text = newsTitleString
        titleLabel.font = titleLabel.font.bold()
        descriptionLabel.text = descString
        editButton.layer.cornerRadius = 5
        deleteButton.layer.cornerRadius = 5
        if !amIAuthor {
            editButton.isHidden = true
            deleteButton.isHidden = true
        }
        setupNavBar()
    }
    
    func setupNavBar() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "left-arrow.png"), style: .plain, target: self, action: #selector(popBack))
    }
    
    @objc func popBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editButtonTapped(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "updateLocalNewsVC") as! UpdateLocalNewsViewController
        vc.titleString = newsTitleString
        vc.descString = descString
        vc.id = id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: "Confirm messgae", message: "Are you sure you want to delete this news?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "YES", style: .default) { (action) in
            if self.sqliteHelper.deleteNews(id: self.id) {
                self.notificationHelper.notify(title: "Success", body: "News deleted successfully")
                self.navigationController?.popViewController(animated: true)
            }
        }
        let cancelAction = UIAlertAction(title: "NO", style: .default) { (action) in
            
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
}
