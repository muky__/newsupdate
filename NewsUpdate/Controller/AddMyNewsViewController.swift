//
//  AddMyNewsViewController.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 3/6/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import UIKit
import UserNotifications

class AddMyNewsViewController: UIViewController, UITextViewDelegate, UNUserNotificationCenterDelegate {
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var addButton: UIButton!
    
    let alertHelper = AlertHelper()
    let notificationHelper = NotificationHelper()
    let sqliteHelper = SQLiteHelper()
    let filledUpChecker = FilledUpChecker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        descriptionTextView.delegate = self
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        } else {
            // Fallback on earlier versions
        }

        setupUI()
        sqliteHelper.connectNewsDB()
        
        if !(UserDefaults.standard.bool(forKey: "isNewsTableCreated")) {
            sqliteHelper.createNewsTable()
        }
    }
    
    func setupUI() {
        setupNavBar()
        titleTextField.borderStyle = .roundedRect
        addButton.layer.cornerRadius = 5
        descriptionTextView.text = "News Description"
        descriptionTextView.textColor = UIColor.lightGray
        
        //adding border in textview as in textfield
        descriptionTextView.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        descriptionTextView.layer.borderWidth = 1.0;
        descriptionTextView.layer.cornerRadius = 5.0;
    }
    
    func setupNavBar() {
        self.title = "Add my news"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "left-arrow.png"), style: .plain, target: self, action: #selector(popBack))
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func popBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "News Description"
            textView.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func addNewsButtonPressed(_ sender: UIButton) {
        if isAllFilledUp() {
            let author = UserDefaults.standard.string(forKey: "username")
            if sqliteHelper.insertNews(title: titleTextField.text!, description: descriptionTextView.text, author: author!) {
                notificationHelper.notify(title: "Success", body: "Your news has been recorded")
                self.navigationController?.popViewController(animated: true)
            }else {
                alertHelper.popAlert(parentViewController: self, title: "Error", message: "News could not be recorded")
            }
        }else {
            alertHelper.popAlert(parentViewController: self, title: "Missing information", message: "Please fill up the whole form")
        }
    }
    
    func isAllFilledUp() -> Bool {
        if (titleTextField.text?.isEmpty)! || (descriptionTextView.text.isEmpty) {
            return false
        }
        return true
    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
}
