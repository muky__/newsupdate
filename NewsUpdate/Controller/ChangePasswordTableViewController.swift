//
//  ChangePasswordTableViewController.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 3/15/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import UIKit
import UserNotifications

class ChangePasswordTableViewController: UITableViewController, UNUserNotificationCenterDelegate {

    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var changePasswordButton: UIButton!
    
    let filledUpChecker = FilledUpChecker()
    let alertHelper = AlertHelper()
    let notificationHelper = NotificationHelper()
    let sqliteHelper = SQLiteHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        sqliteHelper.connectDB()
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        } else {
            // Fallback on earlier versions
        }
    }
    
    func setupUI() {
        oldPasswordTextField.borderStyle = .roundedRect
        newPasswordTextField.borderStyle = .roundedRect
        confirmPasswordTextField.borderStyle = .roundedRect
        changePasswordButton.layer.cornerRadius = 5
        
        setupNavBar()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func setupNavBar() {
        self.title = "Chnage password"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "left-arrow.png"), style: .plain, target: self, action: #selector(popBack))
    }
    
    @objc func popBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changePaswordButtonTapped(_ sender: UIButton) {
        if filledUpChecker.check(textfields: [oldPasswordTextField,
                                              newPasswordTextField,
                                              confirmPasswordTextField]) {
            
            if let userData = UserDefaults.standard.object(forKey: "user") {
                if let user = try? JSONDecoder().decode(User.self, from: userData as! Data) {
                    if oldPasswordTextField.text == user.password {
                        if newPasswordTextField.text == confirmPasswordTextField.text {
                            if newPasswordTextField.text != user.password {
                                if sqliteHelper.updatePassword(username: UserDefaults.standard.string(forKey: "username")!, oldPassword: oldPasswordTextField.text!, newPassword: newPasswordTextField.text!, confirmPassword: confirmPasswordTextField.text!) {
                                        notificationHelper.notify(title: "Success", body: "Password successfully changed")
                                        self.navigationController?.popViewController(animated: true)
                                }else {
                                    alertHelper.popAlert(parentViewController: self, title: "Eroor", message: "Password change operation went wrong")
                                }
                            }else {
                                alertHelper.popAlert(parentViewController: self, title: "Error", message: "Old and new password cannot be same")
                            }
                        } else {
                            alertHelper.popAlert(parentViewController: self, title: "Wrong password", message: "Please match the new password and confirm password field")
                        }
                    } else {
                        alertHelper.popAlert(parentViewController: self, title: "Wrong password", message: "Please enter the correct old password")
                    }
                }
            }
            
        } else {
            alertHelper.popAlert(parentViewController: self, title: "Missing information", message: "Please fill up all the fields")
        }
    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
}
