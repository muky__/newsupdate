//
//  RegisterationViewController.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 3/18/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import UIKit
import UserNotifications

class RegisterationViewController: UIViewController, UNUserNotificationCenterDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var uploadedImageView: UIImageView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var rePasswordTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var contactTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    
    let alertHelper = AlertHelper()
    let notificationHelper = NotificationHelper()
    let filledUpChecker = FilledUpChecker()
    let textFieldCleaner = TextFieldCleaner()
    let validationHelper = ValidationHelper()
    let sqliteHelper = SQLiteHelper()
    
    var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        } else {
            // Fallback on earlier versions
        }
        
        imagePicker.delegate = self
        
        sqliteHelper.connectDB()
        if !(UserDefaults.standard.bool(forKey: "isTableCreated")){
            sqliteHelper.createUserTable()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func setupUI() {
        self.title = "Registeration"
        registerButton.layer.cornerRadius = 5
        resetButton.layer.cornerRadius = 5
        usernameTextField.becomeFirstResponder()
        setupGesture()
        
        usernameTextField.borderStyle = .roundedRect
        passwordTextField.borderStyle = .roundedRect
        rePasswordTextField.borderStyle = .roundedRect
        firstNameTextField.borderStyle = .roundedRect
        lastNameTextField.borderStyle = .roundedRect
        addressTextField.borderStyle = .roundedRect
        emailTextField.borderStyle = .roundedRect
        contactTextField.borderStyle = .roundedRect
    }
    
    func setupGesture() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        uploadedImageView.isUserInteractionEnabled = true
        uploadedImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
//        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        let alert = UIAlertController(title: "Choose Profile Picture", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "Your system does not have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func saveImage(image : UIImage) {
        let data : Data = UIImageJPEGRepresentation(image, 1)!
        UserDefaults.standard.set(data, forKey: "savedImage")
    }
    
    @IBAction func registerButtonClicked(_ sender: UIButton) {
        if filledUpChecker.check(textfields: [
            usernameTextField,
            passwordTextField,
            rePasswordTextField,
            firstNameTextField,
            lastNameTextField,
            addressTextField,
            emailTextField,
            contactTextField]) {
            
            if passwordTextField.text == rePasswordTextField.text {
                if validationHelper.isValidEmail(email: emailTextField.text){
                    let username = usernameTextField.text!
                    let password = passwordTextField.text!
                    let firstName = firstNameTextField.text!
                    let lastName = lastNameTextField.text!
                    let address = addressTextField.text!
                    let email = emailTextField.text!
                    let contact = contactTextField.text!
                    
                    if contact.isPhoneNumber{
                        if !(sqliteHelper.checkUsernameExistence(uname: username)) {
                            if !(sqliteHelper.checkEmailExistence(email: email)) {
                                if !(sqliteHelper.insertUser(username: username, password: password, email: email, contact: contact, firstName: firstName, lastName: lastName, address: address)) {
                                    alertHelper.popAlert(parentViewController: self, title: "Error", message: "User could not be inserted")
                                }else {
                                        saveImage(image: uploadedImageView.image!)
                                        notificationHelper.notify(title: "Success", body: "You are now an official user")
                                        performSegue(withIdentifier: "loginSegue", sender: self)
                                }
                            }else {
                                alertHelper.popAlert(parentViewController: self, title: "Email already used", message: "Please use another email address")
                            }
                        }else {
                            alertHelper.popAlert(parentViewController: self, title: "Username already exists", message: "Please try another username")
                        }
                    }else {
                        alertHelper.popAlert(parentViewController: self, title: "Invalid contact number", message: "Please enter a valid contact number")
                    }
                }else {
                    alertHelper.popAlert(parentViewController: self, title: "Wrong email address", message: "Please enter a valid email address")
                }
            }else {
                alertHelper.popAlert(parentViewController: self, title: "Password mismatch", message: "Please retype the password")
            }
        }else {
            alertHelper.popAlert(parentViewController: self, title: "Missing information", message: "Please fill up all the fields")
        }
    }
    
    @IBAction func resetButtonClicked(_ sender: UIButton) {
        textFieldCleaner.clearAllTextFields(textfields: [
            usernameTextField,
            passwordTextField,
            rePasswordTextField,
            firstNameTextField,
            lastNameTextField,
            addressTextField,
            emailTextField,
            contactTextField
            ])
    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
    
    //MARK: - ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            // imageViewPic.contentMode = .scaleToFill
            uploadedImageView.image = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension String {
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.characters.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.characters.count && self.characters.count == 10
            } else {
                return false
            }
        } catch {
            return false
        }
    }
}
