//
//  NewsFeedTableViewController.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 3/6/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import UIKit

class NewsFeedTableViewController: UITableViewController {
    
    @IBOutlet var newsTableView: UITableView!
    var newsList = [LocalNews]()
    
    let sqliteHelper = SQLiteHelper()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        sqliteHelper.connectNewsDB()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchData()
    }
    
    func setupUI() {
        setupNavBar()
    }
    
    func setupNavBar() {
        self.title = "Local News Feed"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "left-arrow.png"), style: .plain, target: self, action: #selector(popBack))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(pushToAddNews))
    }
    
    @objc func popBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func pushToAddNews() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "addMyNewsVC") as! AddMyNewsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func fetchData() {
        newsList = sqliteHelper.fetchNews()
        newsTableView.reloadData()
        self.newsTableView.tableFooterView = UIView()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return newsList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "localNewsFeedCell", for: indexPath) as! LocalNewsFeedTableViewCell

        cell.titleLabel.text = newsList[indexPath.row].title
        cell.titleLabel.font = cell.titleLabel.font.bold()
//        cell.descriptionLabel.text = newsList[indexPath.row].desc
        cell.authorLabel.text = "Written By: \(newsList[indexPath.row].author ?? "Unknown")"

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "localNewsDetailVC") as! LocalNewsDetailViewController
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "localNewsDetailTVC") as! LocalNewsDetailTableViewController
        vc.newsTitleString = self.newsList[indexPath.row].title!
        vc.descString = self.newsList[indexPath.row].desc!
        vc.id = self.newsList[indexPath.row].id!
        
        if self.newsList[indexPath.row].author != UserDefaults.standard.string(forKey: "username") {
            vc.amIAuthor = false
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
