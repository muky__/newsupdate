//
//  AppDelegate.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 2/28/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import UIKit
import UserNotifications
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.sound, .badge, .alert]) { (success, error) in
                if success {
                    print("Notification has been enabled for the app")
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
        IQKeyboardManager.sharedManager().enable = true
        
        if !(UserDefaults.standard.bool(forKey: "isLoggedIn")) {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "loginVC")
            self.window?.rootViewController = viewController
        }
        
//        let backButtonImage = UIImage(named: "left-arrow.png")
//        let renderedImage = backButtonImage?.stretchableImage(withLeftCapWidth: 15, topCapHeight: 30)
//        UIBarButtonItem.appearance().setBackButtonBackgroundImage(renderedImage, for: .normal, barMetrics: .default)
        
//        let renderedImage = backButtonImage?.withRenderingMode(.alwaysOriginal)
//        UINavigationBar.appearance().backIndicatorImage = renderedImage
//        UINavigationBar.appearance().backIndicatorTransitionMaskImage = renderedImage
//        let barAppearace = UIBarButtonItem.appearance()
//        barAppearace.setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        
//        UIBarButtonItem.appearance().setBackButtonBackgroundImage(backButtonImage, for: .normal, barMetrics: .default)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

