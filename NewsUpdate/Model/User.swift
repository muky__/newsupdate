//
//  User.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 2/28/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import Foundation
class User : Codable{
    var id : Int
    var username : String
    var password : String
    var firstName : String
    var lastName : String
    var address : String
    var email : String
    var contact : String
    
    init(id : Int, username : String, password : String, email : String, contact : String, firstName : String, lastName : String, address : String) {
        self.id = id
        self.username = username
        self.password = password
        self.firstName = firstName
        self.lastName = lastName
        self.address = address
        self.email = email
        self.contact = contact
    }
}
