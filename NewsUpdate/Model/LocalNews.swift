//
//  LocalNews.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 3/6/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import Foundation
class LocalNews : NSObject {
    var id : Int?
    var title : String?
    var desc : String?
    var author : String?
    
    init(id : Int, title : String, desc : String, author : String) {
        self.id = id
        self.title = title
        self.desc = desc
        self.author = author
    }
}
