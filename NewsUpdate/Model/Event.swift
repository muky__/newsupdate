//
//  Event.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 3/18/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import Foundation
class Event : NSObject {

//    "distance": "0.0021036256802834294",
//    "event_id": "111579",
//    "eventName": "Daily Yarra Valley Winery Tours",
//    "eventDescription": "Enjoy a full day of touring carefully selected Yarra Valley Wineries and Vineyards in our air-conditioned vehicles. Our tour includes visits to four cellar doors for wine tastings and a delicious a la",
//    "location": "Melbourne, Victoria, Australia",
//    "latitude": "-37.813610",
//    "longitude": "144.963058",
//    "start_date": "2018-03-01 09:00:00",
//    "end_date": "2018-03-31 18:00:00",
//    "mainImage": "http://res.cloudinary.com/oneplatinumtechnology/image/upload/v1511747611/kso0kxoextrufqh8vall.jpg",
//    "eventPrice": "$130 pp",
//    "isRunning": 1,
//    "hasParticipants": 0,
//    "isUserInterested": 0,
//    "isUserGoing": 0,
//    "hasUserViewedEvent": 0,
//    "isSponsored": 0,
//    "sponsoredTitle": "",
//    "specialevents": "0"
    
    var event_id : String?
    var eventName : String?
    var eventDescription : String?
    var start_date : String?
    var mainImage : String?
    
    init(json : NSDictionary) {
        self.event_id = json["event_id"] as? String
        self.eventName = json["eventName"] as? String
        self.eventDescription = json["eventDescription"] as? String
        self.start_date = json["start_date"] as? String
        self.mainImage = json["mainImage"] as? String
    }
    
}
