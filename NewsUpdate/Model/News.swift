//
//  News.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 2/28/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import Foundation
class News : NSObject {
//    "content_id": "1",
//    "content_title": "Five must-see attractions in Dubai Garden Glow",
//    "content_url": "five-must-see-attractions-in-dubai-garden-glow",
//    "content_page_title": "Five must-see attractions in Dubai Garden Glow",
//    "content_body": "<p style=\"text-align: justify;\">Packing lunches, making sure everyone is on time, ensuring everyone has their pants on, knowing where you&#39;re all headed, dealing with tears, breaking up fights - being a parent in your own house can be difficult. However, there is a way to make things easier, much easier. Take your family on an adventure in the park.</p>\r\n\r\n<p style=\"text-align: justify;\">Dubai Garden Glow can trigger those happy hormones, for starters, the bright lights and colors can increase energy levels and boost happiness. Maybe it&#39;s just the ambiance - who can resist smiling as you see the entire park light up? The park opens its door at 4 pm, it&#39;s the perfect way to get the little ones running around and build up to the light show that starts at sunset.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>#1 Ice Park<br />\r\n#2 My Dubai<br />\r\n#3 Magical Nights<br />\r\n#4 Underwater Wonderland<br />\r\n#5 The Dinosaur Park</strong></p>\r\n",
//    "featured_img": "http://esmart.almawadait.com/uploads/content/212ab20dbdf4191cbcdcf015511783f4aaa.jpg",
//    "meta_description": "",
//    "meta_keywords": "",
//    "meta_title": "",
//    "parent_id": "0",
//    "position": "0",
//    "group_id": "1",
//    "content_type": "News",
//    "publish_status": "1",
//    "delete_status": "0",
//    "category_id": null,
//    "created": "2017-12-10 19:59:04",
//    "updated": null,
//    "content_title_ar": "خمسة معالم يجب أن ترى في دبي حديقة غلو",
//    "content_page_title_ar": "خمسة معالم يجب أن ترى في دبي حديقة غلو",
//    "content_body_ar": "<p style=\"text-align: justify;\">التعبئة وجبات الغداء، والتأكد من أن الجميع في الوقت المحدد، وضمان كل شخص لديه السراويل على، ومعرفة أين أنت جميعا يرأس، والتعامل مع الدموع، وكسر المعارك - كونه أحد الوالدين في منزلك يمكن أن يكون صعبا.&nbsp;ومع ذلك، هناك طريقة لجعل الأمور أسهل، أسهل بكثير.&nbsp;خذ عائلتك في مغامرة في الحديقة.<br />\r\n<br />\r\nدبي حديقة الوهج يمكن أن تؤدي تلك الهرمونات السعيدة، للمبتدئين، يمكن للأضواء الساطعة والألوان زيادة مستويات الطاقة وتعزيز السعادة.&nbsp;ربما انها مجرد أجواء - الذين يمكن أن تقاوم يبتسم كما ترون الحديقة بأكملها تضيء؟&nbsp;الحديقة تفتح أبوابها في الساعة 4 مساء، انها الطريقة المثلى للحصول على القليل منها يركض وبناء المعرض الضوء الذي يبدأ عند غروب الشمس.<br />\r\n<br />\r\n# 1 حديقة الجليد<br />\r\n# 2 دبي<br />\r\n# 3 ليال سحرية<br />\r\n# 4 تحت الماء العجائب<br />\r\n# 5 حديقة الديناصورات</p>"
    
    var content_title : String?
    var content_page_title : String?
    var content_body : String?
    var featured_img : String?
    var created : String?
    
    init(json : NSDictionary) {
        content_title = json["content_title"] as? String
        content_page_title = json["content_page_title"] as? String
        content_body = json["content_body"] as? String
        featured_img = json["featured_img"] as? String
        created = json["created"] as? String
    }
    
}
