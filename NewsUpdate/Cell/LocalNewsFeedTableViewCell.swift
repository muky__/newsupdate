//
//  LocalNewsFeedTableViewCell.swift
//  NewsUpdate
//
//  Created by Mukesh Shakya on 3/6/18.
//  Copyright © 2018 MukY. All rights reserved.
//

import UIKit

class LocalNewsFeedTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
